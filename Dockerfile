FROM maven:3.5.3-jdk-8 as builder
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
WORKDIR /usr/src/app
RUN mvn dependency:go-offline

RUN mvn package

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=builder /usr/src/app/target/*.war /app/boot-rest.war

ENTRYPOINT [ "java", "-jar", "/app/boot-rest.war" ]

